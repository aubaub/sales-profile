from django.apps import AppConfig


class SalesToolConfig(AppConfig):
    name = 'sales_tool'
